//
//  DetailCommonCell.swift
//  searchApp
//
//  Created by 김우성 on 2020/07/24.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class DetailCommonCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!     // 타이틀 라벨
    @IBOutlet weak var subLabel: UILabel!      // 서브 텍스트 라벨

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(text: String, subText: String) {
        self.titleLabel.text = text
        self.subLabel.text = subText
    }
}
