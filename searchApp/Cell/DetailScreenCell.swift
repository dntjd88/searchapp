//
//  DetailScreenCell.swift
//  searchApp
//
//  Created by 김우성 on 2020/07/24.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class DetailScreenCell: UITableViewCell {
    @IBOutlet weak var scrollView:UIScrollView!                 // 좌우스크롤 뷰
    @IBOutlet weak var stackView:UIStackView!                   // 스텍뷰
    @IBOutlet var stackViewWidthConstraint:NSLayoutConstraint!  // 스텍뷰 크기

    let stackViewWidth:CGFloat = 250.0
    var arrayView: Array<DownLoadImageView> = []

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    // 이미지 뷰 추가
    func addScreenShotView(screenshotUrls : Array<String>) {
        if self.arrayView.count == screenshotUrls.count {
            for (index, imageView) in self.arrayView.enumerated() {
                imageView.getImageUrl(stringURL: screenshotUrls[index])
            }
            return
        } else {
            removeScrenShotView()
        }
        
        var toView:DownLoadImageView? = nil
        for screenshotUrl in screenshotUrls {
            if self.arrayView.count > 0 {
                toView = self.arrayView.last
            }
            
            let imageView = DownLoadImageView(frame: CGRect(x: 0, y: 0, width: stackViewWidth, height: self.scrollView.frame.height))
            imageView.layer.masksToBounds = true
            imageView.layer.cornerRadius = 10.0
            self.arrayView.append(imageView)
            self.stackView.addArrangedSubview(imageView)
            
            if toView != nil {
                NSLayoutConstraint(item: imageView,
                                   attribute: .width,
                                   relatedBy: .equal,
                                   toItem: toView,
                                   attribute: .width,
                                   multiplier: 1,
                                   constant: 0).isActive = true
            }
            imageView.getImageUrl(stringURL: screenshotUrl)
        }
        self.stackViewWidthConstraint.constant = stackViewWidth * CGFloat(self.arrayView.count)
    }
    
    // 그리기전 화면 삭제
    func removeScrenShotView() {
        for view in self.arrayView {
            view.removeFromSuperview()
        }
        self.arrayView.removeAll()
    }
}
