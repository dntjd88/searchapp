//
//  DetailTextCell.swift
//  searchApp
//
//  Created by 김우성 on 2020/07/24.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class DetailTextCell: UITableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!   // 텍스트 라벨
    @IBOutlet weak var moreButton: UIButton!        // 더보기 버튼
    @IBOutlet var textViewHeightConstraint:NSLayoutConstraint!  // 텍스트 라벨 높이 제한
    @IBOutlet var textViewTrailingConstraint:NSLayoutConstraint!  // 텍스트 라벨 넓이 간격

    var touchMoreButtonBlock: (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(text: String) {
        self.descriptionLabel.text = text        
    }
    
    // 더보기 버튼 클릭시 텍스트 높이 제한 풀기
    @IBAction func touchMoreButton(sender : UIButton) {
        touchMoreButtonBlock?()
    }
}
