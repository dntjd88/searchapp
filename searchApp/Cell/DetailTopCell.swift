//
//  DetailTopCell.swift
//  searchApp
//
//  Created by 김우성 on 2020/07/24.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class DetailTopCell: UITableViewCell {
    @IBOutlet weak var appImage: DownLoadImageView!       // 앱 이미지
    @IBOutlet weak var trackCensoredNameLabel: UILabel! // 앱이름
    @IBOutlet weak var artistNameLabel: UILabel!        // 만든이

    @IBOutlet weak var openButton: UIButton!            // 열기 버튼
    @IBOutlet weak var shareButton: UIButton!           // 공유 버튼
    
    @IBOutlet weak var averageUserRatingLabel: UILabel! // 별점
    @IBOutlet weak var userRatingCountLabel:UILabel!    // 다운로드 수
    
    @IBOutlet weak var rankingLabel: UILabel!           // #순위
    @IBOutlet weak var genreLabel: UILabel!             // 분류
    @IBOutlet weak var trackContentRatingLabel: UILabel!// 연령

    
    var touchOpenButtonBlock: (() -> Void)? = nil
    var touchShareButtonBlock: (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        // Initialization code
        
        Common.viewRound(view: self.appImage, borderColr: UIColor.lightGray, cornerRadius: 15)
        Common.viewRound(view: self.openButton, cornerRadius: 15)
        Common.viewRound(view: self.shareButton, cornerRadius: 15)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setData(storeDetail: StoreDetail) {
        self.appImage.getImageUrl(stringURL: storeDetail.artworkUrl512)
        self.artistNameLabel.text = storeDetail.artistName
        self.trackCensoredNameLabel.text = storeDetail.trackCensoredName
        
        // 별점
        self.averageUserRatingLabel.text = Common.setStarTextUnit(starCount: storeDetail.averageUserRating)

        // 다운로드 수
        self.userRatingCountLabel.text = Common.setDownloadTextUnit(strCount: storeDetail.userRatingCount) + "개의 평가"
        
        // 분류
        if let genre = storeDetail.genres.first {
            self.genreLabel.text = genre
        }
        
        // 연령
        self.trackContentRatingLabel.text = storeDetail.trackContentRating
    }
    
    @IBAction func touchOpenButton(sender: UIButton) {
        touchOpenButtonBlock?()
    }
    
    @IBAction func touchShareButton(sender: UIButton) {
        touchShareButtonBlock?()
    }
}
