//
//  ResultCell.swift
//  searchApp
//
//  Created by 김우성 on 2020/07/23.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class ResultCell: UITableViewCell {
    var touchOpenButtonBlock: (() -> Void)? = nil

    @IBOutlet weak var appImage:DownLoadImageView!            // 앱 이미지
    @IBOutlet weak var titleLabel:UILabel!              // 앱 타이틀
    @IBOutlet weak var subLabel:UILabel!                // 서브타이틀
    @IBOutlet weak var userRatingCountLabel:UILabel!    // 하단 다운로드 수

    @IBOutlet weak var openButton:UIButton!             // 열기 버튼
    
    @IBOutlet weak var screenShotImage1:DownLoadImageView!    // 스크린샷 이미지1
    @IBOutlet weak var screenShotImage2:DownLoadImageView!    // 스크린샷 이미지2
    @IBOutlet weak var screenShotImage3:DownLoadImageView!    // 스크린샷 이미지3

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none

        Common.viewRound(view: self.appImage, borderColr: UIColor.lightGray, cornerRadius: 10)
        Common.viewRound(view: self.openButton, cornerRadius: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(storeDetail: StoreDetail) {
        // 앱 이미지
        self.appImage.getImageUrl(stringURL: storeDetail.artworkUrl512)
        
        // 앱 타이틀
        self.titleLabel.text = storeDetail.trackName
         
        // 구분
        if let getGenre = storeDetail.genres.first {
            self.subLabel.text = getGenre
        }
        
        // 별점 + 다운로드 수
        self.userRatingCountLabel.text = Common.setStarTextUnit(starCount: storeDetail.averageUserRating) + Common.setDownloadTextUnit(strCount: storeDetail.userRatingCount)
        
        Common.viewRound(view: self.screenShotImage1)
        Common.viewRound(view: self.screenShotImage2)
        Common.viewRound(view: self.screenShotImage3)

        // 스크린샷 이미지 표시
        for (index, value) in storeDetail.screenshotUrls.enumerated() {
            if index == 0 {
                self.screenShotImage1.getImageUrl(stringURL: value, complete: { image in
                    Common.viewRound(view: self.screenShotImage1, borderColr:UIColor.lightGray , cornerRadius: 5)
                    if let tempImage = image {
                        if tempImage.size.width > tempImage.size.height {
                            self.screenShotImage2.isHidden = true
                            self.screenShotImage3.isHidden = true
                        } else {
                            self.screenShotImage2.isHidden = false
                            self.screenShotImage3.isHidden = false
                        }
                    }
                })
            } else if index == 1 {
                self.screenShotImage2.getImageUrl(stringURL: value, complete: { _ in
                    Common.viewRound(view: self.screenShotImage2, borderColr:UIColor.lightGray , cornerRadius: 5)
                })
            } else if index == 2 {
                self.screenShotImage3.getImageUrl(stringURL: value, complete: { _ in
                    Common.viewRound(view: self.screenShotImage3, borderColr:UIColor.lightGray , cornerRadius: 5)
                })
            } else {
                break
            }
        }
        
    }
    
    // 열기 버튼 클릭
    @IBAction func touchOpenButton(sender: UIButton) {
        touchOpenButtonBlock?()
    }
}
