//
//  SearchCell.swift
//  searchApp
//
//  Created by 김우성 on 2020/07/23.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    @IBOutlet weak var titleLabel:UILabel!          // 타이틀

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(title : String) {
        self.titleLabel.text = title
    }

}
