//
//  Common.swift
//  searchApp
//
//  Created by 김우성 on 2020/07/23.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class Common: NSObject {

    struct defaultsKeys {
        static let KeySearchArray = "KeySearchArray"
    }
    
    // url 인코딩
    static func urlEncoding(strUrl:String) -> URL? {
        if let encoded  = strUrl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
            guard let encodingURL = URL(string: encoded) else {
                return nil
            }
            return encodingURL
        }
        return nil
    }
    
    // array 저장
    static func saveDataArray(key : String, saveData: Array<String>) {
        let defaults = UserDefaults.standard
        defaults.set(saveData, forKey: "")
    }
    
    // array 불러오기
    static func loadDataArray(key : String) -> Array<String>? {
        let defaults = UserDefaults.standard
        guard let loadData = defaults.object(forKey: "") as? Array<String> else {
            return nil
        }
        return loadData
    }
    
    
    // url 이동
    static func goToUrl(strUrl:String) {
        guard let url = Common.urlEncoding(strUrl: strUrl) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            print("error")
        }
    }
    
    // 디운로드 수 포멧
    static func setDownloadTextUnit(strCount:String) -> String{
        if let fstrCount = Double(strCount) {
            if strCount.count == 4 {
                let downloadCount = fstrCount / 1000.0
                return String(format: "%.1f천", downloadCount)
            } else if strCount.count >= 5 {
                let downloadCount = fstrCount / 10000.0
                return String(format: "%.0f만", downloadCount)
            } else {
                return strCount
            }
        } else {
            return strCount
        }
    }
    static func setStarTextUnit(starCount:String) -> String{
        var averageUserRating = starCount
        if let fAverageUserRating = Double(starCount) {
           averageUserRating = String(format: "⭐️%.1f ", fAverageUserRating)
        }
        return averageUserRating
    }
    
    // 뷰 round || radius
    static func viewRound(view :UIView, borderColr:UIColor? = nil, cornerRadius:CGFloat? = nil) -> Void {
        if let cornerRadius = cornerRadius {
            view.layer.masksToBounds = true
            view.layer.cornerRadius = cornerRadius
        } else {
            view.layer.masksToBounds = false
            view.layer.cornerRadius = 0.0
        }
        
        if let borderColr = borderColr {
            view.layer.borderWidth = 1.0
            view.layer.borderColor = borderColr.cgColor
        }else {
            view.layer.borderWidth = 0.0
            view.layer.borderColor = UIColor.clear.cgColor
        }
    }
}
