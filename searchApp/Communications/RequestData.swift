//
//  RequestData.swift
//  searchApp
//
//  Created by 김우성 on 2020/07/23.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit
import SwiftyJSON

class RequestData: NSObject {

    static let sharedInstance = RequestData()         // 싱글톤 처리
    // base url
    static let BaseUrlPath = "https://itunes.apple.com"
    // Sub url
    enum SubUrl {
        static let SearchPath = "/search?"
        static let LookUp = "/lookup?"
    }
    
    private let session: URLSession = URLSession.shared
    private var dataTask: URLSessionDataTask?
    
    private let serialQueue = DispatchQueue(label: "request.serial")
    var indicatorView:UIActivityIndicatorView? = nil
    var downLoadQueue = OperationQueue()

    // 결과 조회
    func getSearchAPI(searchKey: String, completion:  @escaping (StoreModel?) -> Void) -> Void{
        let stringURL = RequestData.BaseUrlPath +  SubUrl.SearchPath+"term=\(searchKey)&"+"country=kr&"+"media=software"
        
        guard let url: URL = Common.urlEncoding(strUrl: stringURL) else {
            return completion(nil)
        }
        self.ShowIndicator()
        self.serialQueue.async {
            self.getData(url: url, completion: { data, response, error in
                // 통신 응답 처리
                guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let data = data, error == nil else {
                    DispatchQueue.main.async {
                        completion(nil)
                        self.HideIndicator()
                    }
                    return
                }
                
                // 파서로 데이터 정리
                let jsonValue = JSON(data)
                let model = StoreModel.init(json: jsonValue)

                DispatchQueue.main.async {
                    completion(model)
                    self.HideIndicator()
                }
            })
        }
    }

    // get 방식 요청
    fileprivate func getData(url: URL, completion: @escaping(Data?, URLResponse?, Error?) -> Void) -> Void {
        var request: URLRequest = URLRequest(url: url)
        self.dataTask?.cancel()
        request.httpMethod = "GET"
        self.dataTask = self.session.dataTask(with: request, completionHandler: completion)
        self.dataTask?.resume()
    }
    
    fileprivate func getDownloadData(url: URL, completion: @escaping(Data?, URLResponse?, Error?) -> Void) -> Void {
        var request: URLRequest = URLRequest(url: url)
        request.httpMethod = "GET"
        self.session.dataTask(with: request, completionHandler: completion).resume()
    }
    
    // 요청한 통신 스레드 취소
    func RequestQueueClear() -> Void {
        self.dataTask?.cancel()
        self.downLoadQueue.cancelAllOperations()
    }
    
    //MARK: - Indicator
    fileprivate func ShowIndicator() -> Void{
        DispatchQueue.main.async {
            let window = UIApplication.shared.delegate?.window!
            let windowSize:CGRect = window!.frame
            if self.indicatorView == nil {
                self.indicatorView = UIActivityIndicatorView.init(style: .gray)
                self.indicatorView?.frame = windowSize
                self.indicatorView?.hidesWhenStopped = true
                self.indicatorView?.backgroundColor = UIColor.clear
                window?.addSubview(self.indicatorView!)
                self.indicatorView?.startAnimating()
            }else {
                window?.bringSubviewToFront(self.indicatorView!)
                self.indicatorView?.startAnimating()
            }
        }
    }
    
    fileprivate func HideIndicator() -> Void {
        self.indicatorView?.stopAnimating()
    }
}

// 이미지 캐싱 처리
let imageCache = NSCache<NSString, UIImage>()

//MARK: - UIImageView download
class DownLoadImageView: UIImageView {
    var imageUrlString: String?
    
    func getImageUrl(stringURL: String, complete: ((UIImage?) -> Void)? = nil) -> Void {
        self.imageUrlString = stringURL
        self.image = nil
        guard let url: URL = Common.urlEncoding(strUrl: stringURL) else {
            return
        }
        if let cacheImage = imageCache.object(forKey: url.absoluteString as NSString) {
            self.image = cacheImage
            if complete != nil {
                complete!(cacheImage)
            }
            return
        } else {
            // OperationQueue 우선순위는 기본 utility
            RequestData.sharedInstance.downLoadQueue.addOperation {
                RequestData.sharedInstance.getDownloadData(url: url, completion: { [weak self] data, response, error in
                        // 통신 응답 처리
                    guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                        let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                        let data = data, error == nil else {
                            return
                    }
                
                    OperationQueue.main.addOperation {
                        if let image = UIImage(data: data) {
                            imageCache.setObject(image, forKey: url.absoluteString as NSString)
                            if let weakSelf = self {
                                if weakSelf.imageUrlString == url.absoluteString {
                                    weakSelf.image = image
                                    if complete != nil {
                                        complete!(image)
                                    }
                                }
                            }
                        }
                    }
                })
            }
        }
    }
}
