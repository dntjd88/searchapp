//
//  SearchDetailViewController.swift
//  searchApp
//
//  Created by 김우성 on 2020. 7. 23..
//  Copyright © 2020년 김우성. All rights reserved.
//

import UIKit


fileprivate class TableData {
    var title:String? = ""
    var listCellData: Array<CellData> = []
    init?(title:String? = nil, cellData: Array<CellData>) {
        self.title = title
        self.listCellData = cellData
    }
}

fileprivate class CellData {
    var tableStyle:String = ""
    var text:String = ""
    var subText:String = ""

    init?(style:String!, text: String = "", subText:String = "") {
        self.tableStyle = style
        self.text = text
        self.subText = subText
    }
}

class SearchDetailViewController: UIViewController {
    
    var storeDetail:StoreDetail? = nil
    fileprivate var listData: Array<TableData> = []
    @IBOutlet weak var detailTableView:UITableView? = nil
    
    private let HeaderHeight:CGFloat = 40.0

    fileprivate enum TableStyle {
        static let Top = "DetailTopCell"           // 앱이미지 가격, 버튼 표시 Cell
        static let Screen = "DetailScreenCell"     // 스크린 샷 이미지 표시 Cell
        static let Common = "DetailCommonCell"           // 양쪽 텍스트 표시 Cell
        static let Text = "DetailTextCell"         // Text 표시 Cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    // 데이터 셋팅
    func setDetailData(detailData: StoreDetail) {
        self.listData.removeAll()
        self.storeDetail = detailData
        
        guard let getDetailData = self.storeDetail else {
            return
        }
        var oneRowData: Array<CellData> = []
        
        // 1. 앱, 가격, 버튼 표시 Cell
        if let rowData = CellData.init(style: TableStyle.Top) {
            oneRowData.append(rowData)
        }
        
        if let headerData = TableData.init(cellData: oneRowData) {
            self.listData.append(headerData)
        }
        
        // 2. 새로운 기능
        var twoRowData: Array<CellData> = []
        if let rowData = CellData.init(style: TableStyle.Text, text: getDetailData.releaseNotes) {
            twoRowData.append(rowData)
        }
        
        if let headerData = TableData.init(title: "새로운 기능", cellData: twoRowData) {
            self.listData.append(headerData)
        }

        // 3. 스크린샷
        var threeRowData: Array<CellData> = []
        if let rowData = CellData.init(style: TableStyle.Screen) {
            threeRowData.append(rowData)
        }
        
        if let headerData = TableData.init(cellData: threeRowData) {
            self.listData.append(headerData)
        }

        // 4. 설명
        var fourRowData: Array<CellData> = []
        if let rowData = CellData.init(style: TableStyle.Text, text: getDetailData.description) {
            fourRowData.append(rowData)
        }

        if let headerData = TableData.init(cellData: fourRowData) {
            self.listData.append(headerData)
        }

        // 5. 기타 정보
        var fiveRowData: Array<CellData> = []
        let megaByte = CGFloat(NSString(string: getDetailData.fileSizeBytes).floatValue) / 1024 / 1024
        let strMegaByte = String(format: "%.1f MB", megaByte)
        if let rowData = CellData.init(style: TableStyle.Common, text: "크기", subText: strMegaByte) {
            fiveRowData.append(rowData)
        }
        if let rowData = CellData.init(style: TableStyle.Common, text: "연령", subText: getDetailData.trackContentRating) {
            fiveRowData.append(rowData)
        }
        if let headerData = TableData.init(title: "정보", cellData: fiveRowData) {
            self.listData.append(headerData)
        }
        
        self.detailTableView?.reloadData()
    }
    
    
    //MARK: - Button Event
    // 웹에서 보기
    func goToWebLink() {
        guard let getDetailData = self.storeDetail else {
            return
        }
        Common.goToUrl(strUrl: getDetailData.trackViewUrl)
    }
    
    // 공유하기
    func goToShare() {
        guard let getDetailData = self.storeDetail else {
            return
        }
        
        guard let trackViewUrl = URL(string: getDetailData.trackViewUrl) else {
            return
        }
        
        let shareData = [ trackViewUrl ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareData, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view // 안넣으면 iPad 크래쉬 난다고함
        // 제외하고 싶은 타입을 설정
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.saveToCameraRoll, // 사진첩
            UIActivity.ActivityType.assignToContact,   // 전화번호부
        ]

        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil)
        }
        //Completion handler
        activityViewController.completionWithItemsHandler = { (activityType: UIActivity.ActivityType?, completed:
        Bool, arrayReturnedItems: [Any]?, error: Error?) in
            if completed {
                print("share completed")
                return
            } else {
                print("cancel")
            }
            if let shareError = error {
                print("error while sharing: \(shareError.localizedDescription)")
            }
        }
    }
    
    func setTableCellUpdate(indexPath: IndexPath) {
    }
}

//MARK: - TableView Event
extension SearchDetailViewController: UITableViewDelegate, UITableViewDataSource {
    // 헤더 뷰 높이
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let tableData:TableData = listData[section]
        if tableData.title != nil {
            return HeaderHeight
        } else {
            return 0
        }
    }

    // 헤더 뷰
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tableData:TableData = listData[section]
        if let titleText = tableData.title {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: Int(HeaderHeight)))
            headerView.backgroundColor = UIColor.white
            let titlelabel = UILabel(frame: CGRect(x: 10, y: 15, width: headerView.bounds.width-10, height: headerView.bounds.height-15))
            titlelabel.font = UIFont.boldSystemFont(ofSize: 20)
            titlelabel.text = titleText
            headerView .addSubview(titlelabel)
            // add border line
            let topBorder:CALayer = CALayer()
            topBorder.frame = CGRect.init(x: 15, y: 0, width: headerView.bounds.width, height: 0.5)
            topBorder.backgroundColor = tableView.separatorColor?.cgColor
            headerView.layer.addSublayer(topBorder)
            return headerView
        } else {
            return nil
        }
    }
    
    // 헤더 갯수
    func numberOfSections(in tableView: UITableView) -> Int {
        return listData.count
    }
    
    // 로우 갯수
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let tableData:TableData = listData[section]
        return  tableData.listCellData.count
    }
    
    // 셀 그리기
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableData:TableData = listData[indexPath.section]
        let cellData:CellData = tableData.listCellData[indexPath.row]
        
        if cellData.tableStyle == TableStyle.Top {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableStyle.Top, for: indexPath) as! DetailTopCell
            
            guard let getDetailData = self.storeDetail else {
                return cell
            }

            cell.setData(storeDetail: getDetailData)
            cell.touchOpenButtonBlock = {
                self.goToWebLink()
            }
            
            cell.touchShareButtonBlock = {
                self.goToShare()
            }
            
            return cell
        } else if cellData.tableStyle == TableStyle.Screen {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableStyle.Screen, for: indexPath) as! DetailScreenCell
            guard let getDetailData = self.storeDetail else {
                return cell
            }
            cell.addScreenShotView(screenshotUrls: getDetailData.screenshotUrls)
        
            return cell
        }else if cellData.tableStyle == TableStyle.Common {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableStyle.Common, for: indexPath) as! DetailCommonCell
            cell.setData(text: cellData.text, subText: cellData.subText)

            return cell
        } else if cellData.tableStyle == TableStyle.Text {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableStyle.Text, for: indexPath) as! DetailTextCell
            cell.setData(text: cellData.text)
            cell.touchMoreButtonBlock = {
                tableView.beginUpdates()
                cell.textViewHeightConstraint.isActive = false
                cell.moreButton.isHidden = true
                tableView.endUpdates()
            }
            return cell
        }

        return UITableViewCell()
    }
}

