//
//  SearchViewController.swift
//  searchApp
//
//  Created by 김우성 on 2020/07/23.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var cancelButton:UIButton!
    
    @IBOutlet var resultTableView:UITableView!
    @IBOutlet var searchTableView:UITableView!
    
    
    var saveSearchData:Array<String> = []
    var searchCompletionData:Array<String> = []
    
    var resultData:StoreModel? = nil

    var searchKey = ""
    private let HeaderHeight:CGFloat = 30.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "검색"
        
        // 테이블 데이터 없을때 라인 지우기
        let searchfooterView = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        searchTableView.tableFooterView = searchfooterView
        
        self.searchTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        // 처음 진입시 결과 테이블 숨김
        self.resultTableView.isHidden = true
        if let saveData = Common.loadDataArray(key: Common.defaultsKeys.KeySearchArray) {
            self.saveSearchData = saveData
            self.searchTableView.reloadData()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // 통신 요청
    func requestData(searchKey: String){
        self.resultData?.removeAll()

        self.searchKey = searchKey
        self.searchBar.resignFirstResponder()
        self.searchTableView.isHidden = true

        if self.saveSearchData.contains(searchKey) == false {
            self.saveSearchData.insert(searchKey, at: 0)
            Common.saveDataArray(key: Common.defaultsKeys.KeySearchArray, saveData: self.saveSearchData)
        }
        
        RequestData.sharedInstance.getSearchAPI(searchKey: searchKey, completion: {
            [weak self] data in
            if data != nil {
                self?.searchTableView.isHidden = true
                self?.resultTableView.isHidden = false
                self?.resultData = data!
                self?.resultTableView.reloadData()
            } else {
                print("request error")
                self?.resultTableView.reloadData()
            }
        })
    }
}


//MARK: - TableView Event
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    // 테이블 Header Height
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.searchTableView && self.searchBar.text?.count == 0 {
            return HeaderHeight
        } else {
            return 0
        }
    }
    
    // 테이블 Header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == self.searchTableView && self.searchBar.text?.count == 0 {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: Int(HeaderHeight)))
            headerView.backgroundColor = UIColor.white
            let titlelabel = UILabel(frame: CGRect(x: 10, y: 0, width: headerView.bounds.width-10, height: headerView.bounds.height))
            titlelabel.font = UIFont.boldSystemFont(ofSize: 30)
            titlelabel.text = "최근 검색어"
            headerView .addSubview(titlelabel)
            return headerView
        } else {
            return nil
        }
    }

    // 테이블 Cell Count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchTableView {
            if self.searchBar.text?.count == 0 {
                return self.saveSearchData.count
            } else {
                return self.searchCompletionData.count
            }
        } else {
            return self.resultData?.count ?? 0
        }
    }
    
    // 테이블 Cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        if tableView == self.searchTableView {
            if self.searchBar.text?.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
                cell.textLabel?.text = self.saveSearchData[row]
                cell.selectionStyle = .none
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
                cell.setData(title: self.searchCompletionData[row])

                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell", for: indexPath) as! ResultCell
            guard let cellData = self.resultData?.storeArray[row] else {
                return cell
            }
            
            cell.setData(storeDetail: cellData)
            cell.touchOpenButtonBlock = {
                Common.goToUrl(strUrl: cellData.trackViewUrl)
            }
            
            return cell
        }
    }
    
    // 테이블 선택
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.searchTableView {
            // 검색어 선택시 검색 Request 호출
            var searchText = ""
            if self.searchBar.text?.count == 0 {
                searchText = self.saveSearchData[indexPath.row]
                self.searchBar.text = searchText
            } else {
                searchText = self.searchCompletionData[indexPath.row]
                self.searchBar.text = searchText
            }
            
            self.requestData(searchKey: searchText)
        } else {
            // 검색 결과 선택시 상세 페이지 이동
            guard let cellData = self.resultData?.storeArray[indexPath.row] else {
                print("data not found")
                return
            }
            let detailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchDetailViewController") as! SearchDetailViewController
            detailViewController.setDetailData(detailData: cellData)
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }
}

//MARK: - Search Bar Event
extension SearchViewController: UISearchBarDelegate, UITextFieldDelegate {
    // 텍스트 입력
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchTableView.isHidden = false
        self.resultTableView.isHidden = true
        
        if searchText == "" {
            self.searchCompletionData.removeAll()
            self.searchTableView.reloadData()
        } else {
            DispatchQueue.global().async {
                let searchPredicate = NSPredicate(format: "SELF CONTAINS %@", searchText)
                self.searchCompletionData = (self.saveSearchData as NSArray).filtered(using: searchPredicate) as! Array<String>
                DispatchQueue.main.async {
                    self.searchTableView.reloadData()
                }
            }
        }
    }
    
    // 취소 버튼 클릭
    @IBAction func touchCancelButton (sender : UIButton) {
        if self.searchBar.canBecomeFirstResponder {
            self.searchTableView.isHidden = false
            self.resultTableView.isHidden = true
            self.searchBar.text = ""
            self.searchCompletionData.removeAll()
            self.searchTableView.reloadData()
        } else {
            self.searchBar.resignFirstResponder()
        }
    }
    
    // 키보드 검색 버튼 클릭
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = self.searchBar.text else {
            self.searchBar.resignFirstResponder()
            return
        }
        self.requestData(searchKey: text)
    }
}
